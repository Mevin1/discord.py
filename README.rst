discord.py life support
==========

The newest version of discord.py that works with the current version of PCMRBot.

Really I just went through every commit of the discord.py master branch until PCMRBot broke, then decided to just stick with the commit right before that.

It used to be enough to just pull directly from that commit, but now with Discord adding Display Names, I needed to extend it manually without breaking anything else.